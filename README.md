## Advance MySQL Database Backup Script

In this repository, you will get an advance MySQL database backup script (docker).

## Require environment

- docker

### Clone this repository

Clone this repository under **/etc** directory.

> cd /etc/

> git clone https://gitlab.com/vddongbk/backup-script

### Configure setup

Run the following command to copy config file

> cp .env.example .env

Open file .env and update info variables

### Execute backup script

Run the following commands step by step to execute this script.

> cd /etc/backup-script

> chmod a+x scriptdump.sh

> ./scriptdump.sh

### Schedule daily cron

You can also schedule this to run on daily basis using crontab. Add the following settings to crontab to run on 2:00 AM daily.

> 0 2 \* \* \* cd /etc/backup-script && ./mysqldump.sh > /dev/cron_db_backup.log 2>&1

GOOD LUCK!!!
