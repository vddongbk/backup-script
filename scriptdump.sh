#!/bin/bash

##########################################################################
###
###      Author: @dongvd
###      Version 1.0.0
###
##########################################################################

CONFIGFILE=./.env

source $CONFIGFILE
TIME_FORMAT='%d%m%Y-%H%M'
cTime=$(date +"${TIME_FORMAT}")
LOGFILENAME=$LOG_PATH/mydumpadmin-${cTime}.txt
[ ! -d $LOG_PATH ] && ${MKDIR} -p ${LOG_PATH}

FILE_NAME=""

if [ ${TYPE_DATABASE} == "mysql" ];
then
    FILE_NAME="${MYSQL_DATABASE}.${cTime}.sql.gz";
elif [ ${TYPE_DATABASE} == "mongo" ];
then
    FILE_NAME="${MONGO_DATABASE}.${cTime}";
fi

FILE_PATH="${LOCAL_BACKUP_DIR}"
FILENAMEPATH="$FILE_PATH$FILE_NAME"
[ ! -d $FILE_PATH ] && ${MKDIR} -p $FILE_PATH

echo "" > ${LOGFILENAME}
echo "<<<<<<   Database Dump Report :: `date +%D`  >>>>>>" >> ${LOGFILENAME}
echo "" >> ${LOGFILENAME}

### close_on_error on demand with message ###
close_on_error() {
    echo "$@" ${LOGFILENAME}
    exit 99
}

check_config() {
    [ ! -f $CONFIGFILE ] && close_on_error "Config file not found, make sure config file is correct"
}

mysqldump() {
    echo "Gonna create dump file: $FILENAMEPATH" >> ${LOGFILENAME}

    if [ ${BACKUP_TYPE} == "docker" ]; then
        docker exec ${MYSQL_DOCKER_NAME} ${MYSQLDUMP_DOCKER} -u ${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} ${MYSQL_DATABASE} | gzip > $FILENAMEPATH
    else
        ${MYSQLDUMP_LOCAL} -u ${MYSQL_USERNAME} --password=${MYSQL_PASSWORD} ${MYSQL_DATABASE} | gzip > $FILENAMEPATH
    fi

    if [ $? -eq 0 ]; then
        echo "App database dump created OK. File: $FILENAMEPATH" >> ${LOGFILENAME}
    else
        echo "App database dump FAILED!" >> ${LOGFILENAME}
        exit 1
    fi
}

mongodump() {
    echo "Gonna create dump file: $FILENAMEPATH" >> ${LOGFILENAME}
    
    ${MONGODUMP_LOCAL} --host ${MONGO_HOST} --port ${MONGO_PORT} --db ${MONGO_DATABASE} --username ${MONGO_USERNAME} --password ${MONGO_PASSWORD} --gzip --archive=$FILENAMEPATH.gz

    if [ $? -eq 0 ]; then
        echo "App database dump created OK. File: $FILENAMEPATH" >> ${LOGFILENAME}
    else
        echo "App database dump FAILED!" >> ${LOGFILENAME}
        exit 1
    fi
}

s3_backup() {

    echo "Gonna upload dump file to S3: $FILENAMEPATH" >> ${LOGFILENAME}

    if [ ${TYPE_DATABASE} == "mysql" ];
    then
        docker run --rm -v "${FILE_PATH}:/project" metala/digitalocean-s3cmd --host=${S3_ENDPOINT} --host-bucket="%(bucket)s.${S3_ENDPOINT}" --access_key=${S3_ACCESS_KEY} --secret_key=${S3_SECRET_KEY} put /project/${FILE_NAME} s3://${S3_BUCKET}/${S3_PATH_BACKUP}
    elif [ ${TYPE_DATABASE} == "mongo" ];
    then
        docker run --rm -v "${FILE_PATH}:/project" metala/digitalocean-s3cmd --host=${S3_ENDPOINT} --host-bucket="%(bucket)s.${S3_ENDPOINT}" --access_key=${S3_ACCESS_KEY} --secret_key=${S3_SECRET_KEY} put /project/${FILE_NAME}.gz s3://${S3_BUCKET}/${S3_PATH_BACKUP}
    fi

    if [ $? -eq 0 ]; then
        echo "Upload to s3 OK. File: $FILENAMEPATH" >> ${LOGFILENAME}
    else
        echo "Upload to s3 FAILED!" >> ${LOGFILENAME}
        exit 1
    fi
    
}

remove_file_dump() {
    echo "Remove dump file: $FILENAMEPATH" >> ${LOGFILENAME}
    rm -rf $FILENAMEPATH
    rm -rf $FILENAMEPATH.gz
    if [ $? -eq 0 ]; then
        echo "Remove dump OK. File: $FILENAMEPATH" >> ${LOGFILENAME}
    else
        echo "Remove dump FAILED!" >> ${LOGFILENAME}
        exit 1
    fi
}

notify_end() {
    echo "Operation Dump_and_Upload completed Successfully!" >> ${LOGFILENAME}
}

dump() {
    if [ ${TYPE_DATABASE} == "mysql" ];
    then
        mysqldump;
    elif [ ${TYPE_DATABASE} == "mongo" ];
    then
        mongodump;
    fi
}

# exucute run
check_config
dump
s3_backup
remove_file_dump
notify_end